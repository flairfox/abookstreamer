package ru.flairfox.tech.abookstreamer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AbookstreamerApplication

fun main(args: Array<String>) {
    runApplication<AbookstreamerApplication>(*args)
}
