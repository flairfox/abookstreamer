package ru.flairfox.tech.abookstreamer.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.flairfox.tech.abookstreamer.domain.SyncData
import java.util.*

interface SyncDataRepository : JpaRepository<SyncData, UUID>