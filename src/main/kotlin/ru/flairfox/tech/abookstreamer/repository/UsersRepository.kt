package ru.flairfox.tech.abookstreamer.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.flairfox.tech.abookstreamer.domain.User
import java.util.*

@Repository
interface UsersRepository : JpaRepository<User, UUID>