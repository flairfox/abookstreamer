package ru.flairfox.tech.abookstreamer.domain

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "sync_data")
data class SyncData(

        @Id
        val audiobookId: UUID,

        @Column
        val fileName: String,

        @Column
        val position: Number)