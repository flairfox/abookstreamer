package ru.flairfox.tech.abookstreamer.domain

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "users")
data class User(
        @Id
        val id: UUID,

        @Column
        val username: String,

        @Column
        val password: String
)