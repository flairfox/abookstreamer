package ru.flairfox.tech.abookstreamer.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ru.flairfox.tech.abookstreamer.collection.Audiobook
import ru.flairfox.tech.abookstreamer.collection.Library
import ru.flairfox.tech.abookstreamer.domain.SyncData
import ru.flairfox.tech.abookstreamer.repository.SyncDataRepository

@RestController
@RequestMapping("/data")
class DataController(
        private val syncDataRepository: SyncDataRepository,
        private val library: Library) {

    @GetMapping("/audiobook")
    fun audiobook(): ResponseEntity<Audiobook> {
        val audiobook = library.collection[0]
        audiobook.syncData = syncDataRepository.findById(audiobook.id).orElse(null)

        return ResponseEntity(audiobook, HttpStatus.OK)
    }

    @PostMapping("/sync")
    fun sync(@RequestBody syncData: SyncData) {
        syncDataRepository.save(syncData)
    }

}