package ru.flairfox.tech.abookstreamer.controllers

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class ApplicationController {

    @RequestMapping("/")
    fun application(model: Model): String {
        return "index"
    }

}