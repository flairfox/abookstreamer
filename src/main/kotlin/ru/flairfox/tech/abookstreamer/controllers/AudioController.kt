package ru.flairfox.tech.abookstreamer.controllers

import org.springframework.core.io.UrlResource
import org.springframework.core.io.support.ResourceRegion
import org.springframework.http.*
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import ru.flairfox.tech.abookstreamer.collection.Library
import java.util.*
import kotlin.math.min

@RestController
class AudioController(
        private val library: Library
) {

    @GetMapping("/audio/{bookId}/{fileName:.+}")
    fun getAudio(@PathVariable bookId: UUID,
                 @PathVariable fileName: String,
                 @RequestHeader headers: HttpHeaders): ResponseEntity<UrlResource> {

        val audiobook = library.findBook(bookId)
        val audio = UrlResource("file://${audiobook.folder}/$fileName")
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaTypeFactory
                        .getMediaType(audio)
                        .orElse(MediaType.APPLICATION_OCTET_STREAM))
                .body(audio)
    }
    
}