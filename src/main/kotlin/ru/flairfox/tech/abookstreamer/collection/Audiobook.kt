package ru.flairfox.tech.abookstreamer.collection

import ru.flairfox.tech.abookstreamer.domain.SyncData
import java.nio.file.Path
import java.util.*

data class Audiobook(
        val id: UUID,
        @Transient
        val folder: Path,
        val cover: Path?,
        val title: String?,
        val author: String?,
        val performer: String?,
        val year: String?,
        val tracks: List<AudiobookTrack>,
        var syncData: SyncData? = null)