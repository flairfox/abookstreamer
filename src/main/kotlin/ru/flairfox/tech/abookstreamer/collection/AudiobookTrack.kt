package ru.flairfox.tech.abookstreamer.collection

import java.nio.file.Path

data class AudiobookTrack(
        val folder: Path,
        val fileName: String,
        val duration: Int,
        val title: String?,
        val author: String?,
        val performer: String?,
        val year: String?,
        val number: String?
)