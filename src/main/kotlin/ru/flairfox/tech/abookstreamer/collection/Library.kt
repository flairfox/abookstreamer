package ru.flairfox.tech.abookstreamer.collection

import org.springframework.stereotype.Component
import java.util.*

@Component
class Library(
        scanner: Scanner
) {
    val collection = scanner.scanLibrary()

    fun findBook(id: UUID): Audiobook {
        return collection.find { audiobook -> audiobook.id == id } as Audiobook
    }
}
