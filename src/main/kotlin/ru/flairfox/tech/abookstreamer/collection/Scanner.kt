package ru.flairfox.tech.abookstreamer.collection

import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.audio.exceptions.CannotReadException
import org.jaudiotagger.tag.FieldKey
import org.springframework.stereotype.Component
import ru.flairfox.tech.abookstreamer.utils.parallelMap
import java.io.FileReader
import java.io.FileWriter
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes
import java.util.*

private const val COVER_FILE = "cover"
private const val METADATA_FOLDER = ".abookstreamer"
private const val ID_FILE = "book_id"

@Component
class Scanner {

    private val libraryPath = Path.of("/home/flairfox/Audiobooks")

    fun scanLibrary(): List<Audiobook> {
        return scan(libraryPath)
                .parallelMap { path -> buildTrack(path) }
                .filterNotNull()
                .groupBy { audiobookTrack -> audiobookTrack.folder }
                .map { entry -> buildAudiobook(entry.key, entry.value) }
    }

    private fun scan(vararg basePaths: Path): List<Path> {
        val foundPaths = mutableListOf<Path>()
        basePaths.forEach { basePath ->
            Files.walkFileTree(basePath, object : SimpleFileVisitor<Path>() {
                override fun visitFile(filePath: Path, attrs: BasicFileAttributes?): FileVisitResult {
                    foundPaths.add(filePath)
                    return super.visitFile(filePath, attrs)
                }
            })
        }

        return foundPaths;
    }

    private fun buildTrack(path: Path): AudiobookTrack? {

        val (tags, length) = try {
            val audioFile = AudioFileIO.read(path.toFile())
            Pair(audioFile.tag, audioFile.audioHeader.trackLength)
        } catch (e: CannotReadException) {
            return null
        }

        val folder = path.parent

        val titleTag = tags.getFirst(FieldKey.ALBUM)
        val authorTag = tags.getFirst(FieldKey.COMPOSER)
        val performerTag = tags.getFirst(FieldKey.ARTIST)
        val trackTag = tags.getFirst(FieldKey.TRACK)
        val yearTag = tags.getFirst(FieldKey.YEAR)

        return AudiobookTrack(
                folder,
                path.fileName.toString(),
                length,
                if (titleTag.isNullOrBlank()) null else titleTag,
                if (authorTag.isNullOrBlank()) null else authorTag,
                if (performerTag.isNullOrBlank()) null else performerTag,
                if (yearTag.isNullOrBlank()) null else yearTag,
                if (trackTag.isNullOrBlank()) null else trackTag)
    }

    private fun buildAudiobook(folder: Path, tracks: List<AudiobookTrack>): Audiobook {

        val metaFolderPath = folder.resolve(METADATA_FOLDER)
        val idFilePath = metaFolderPath.resolve(ID_FILE)

        val id = if (Files.exists(idFilePath)) {
            FileReader(idFilePath.toFile()).use { fileReader ->
                return@use UUID.fromString(fileReader.readText())
            }
        } else {
            val newId = UUID.randomUUID()
            Files.createDirectories(metaFolderPath)
            FileWriter(idFilePath.toFile()).use { fileWriter ->
                fileWriter.append(newId.toString())
            }
            newId
        }

        val cover = Files.list(folder)
                .filter { path -> path.fileName.toString().toLowerCase().contains(COVER_FILE) }
                .findFirst()
                .orElse(null)

        val title = tracks
                .find { audiobookTrack -> audiobookTrack.title != null }
                ?.title
        val author = tracks
                .find { audiobookTrack -> audiobookTrack.author != null }
                ?.author
        val performer = tracks
                .find { audiobookTrack -> audiobookTrack.performer != null }
                ?.performer
        val year = tracks
                .find { audiobookTrack -> audiobookTrack.year != null }
                ?.year


        return Audiobook(id, folder, cover, title, author, performer, year, tracks.sortedBy { it.fileName })
    }

}