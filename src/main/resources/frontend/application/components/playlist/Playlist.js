angular
    .module('applicationModule')
    .component('playlist', {
        templateUrl: 'frontend/application/components/playlist/Playlist.html',
        bindings: {
            audiobook: "<"
        },
        controller: PlaylistController
    });

const NOT_LOADED = 'NOT_LOADED';
const LOADED = 'LOADED';
const LOADING = 'LOADING';
const PLAYING = 'PLAYING';
const PAUSED = 'PAUSED';
const FINISHED = 'FINISHED';
const LOAD_ERROR = 'LOAD_ERROR';
const PLAY_ERROR = 'PLAY_ERROR';

const PRELOAD_THRESHOLD = 180;

function PlaylistController($scope, $interval, $http) {
    let ctrl = this;

    ctrl.progress = 0;
    let isSeek = false;
    ctrl.startSeek = function () {
        isSeek = true;
    }
    ctrl.stopSeek = function () {
        isSeek = false;
        ctrl.currentTrack.audio.seek(ctrl.progress);
    }
    ctrl.formatDuration = function (duration) {
        let minutes = Math.floor(duration / 60) || 0;
        let seconds = Math.floor(duration - minutes * 60) || 0;
        return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
    }

    ctrl.volumeLevel = 50;
    ctrl.changeVolume = function () {
        if (ctrl.currentTrack) {
            ctrl.currentTrack.audio.volume(ctrl.volumeLevel / 100)
        }
    }

    $interval(function () {
        if (ctrl.currentTrack) {
            $http({
                method: 'POST',
                url: '/data/sync',
                data: {
                    audiobookId: ctrl.audiobookId,
                    fileName: ctrl.currentTrack.fileName,
                    position: ctrl.currentTrack.position
                }
            });
        }
    }, 3000)

    let playPrev = function (audiobookTrack) {
        if (audiobookTrack) {
            let index = audiobookTrack.index - 1;
            if (index >= 0) {
                let prevTrack = ctrl.trackList[index]
                playPauseTrack(prevTrack);
            }
        }
    }
    ctrl.playPrev = playPrev;

    let playNext = function (audiobookTrack) {
        if (audiobookTrack) {
            let index = audiobookTrack.index + 1;
            if (index <= ctrl.trackList.length) {
                let nextTrack = ctrl.trackList[index]
                playPauseTrack(nextTrack);
            }
        }
    }
    ctrl.playNext = playNext;

    let playPauseTrack = function (audiobookTrack, syncData) {
        if (audiobookTrack) {
            if (audiobookTrack === ctrl.currentTrack) {
                if (audiobookTrack.state === PLAYING) {
                    audiobookTrack.audio.pause();
                } else if (audiobookTrack.state === PAUSED) {
                    audiobookTrack.audio.play();
                }

                return;
            }

            if (ctrl.currentTrack) stopTrack(ctrl.currentTrack);
            if (ctrl.nextTrack && ctrl.nextTrack !== audiobookTrack) unloadTrack(ctrl.nextTrack)
            ctrl.nextTrack = null;

            if (audiobookTrack.state === NOT_LOADED)
                loadTrack(
                    audiobookTrack,
                    function () {
                        playTrack(audiobookTrack, syncData);
                    });
            else
                playTrack(audiobookTrack, syncData);
        }
    }
    ctrl.playPauseTrack = playPauseTrack;

    let playTrack = function (audiobookTrack, syncData) {
        if (audiobookTrack && audiobookTrack.audio) {

            ctrl.currentTrack = audiobookTrack;

            audiobookTrack.audio.volume(ctrl.volumeLevel / 100);
            if (syncData) {
                audiobookTrack.audio.seek(syncData.position);
                audiobookTrack.position = syncData.position;
                ctrl.progress = syncData.position;
                audiobookTrack.audio.pause();
            } else {
                audiobookTrack.audio.play();
            }

            audiobookTrack.timer = $interval(function () {
                audiobookTrack.position = audiobookTrack.audio.seek();
                if (!isSeek) ctrl.progress = audiobookTrack.position;
                if (!ctrl.nextTrack
                    && (audiobookTrack.duration - audiobookTrack.position) < PRELOAD_THRESHOLD) {
                    prepareNextTrack(audiobookTrack);
                }
            }, 1000);
        }
    }

    let stopTrack = function (audiobookTrack) {
        if (audiobookTrack) {
            if (audiobookTrack.audio) audiobookTrack.audio.stop();
            unloadTrack(audiobookTrack);
        }
    }

    let loadTrack = function (audiobookTrack, successCallback, errorCallback) {
        if (audiobookTrack) {
            audiobookTrack.state = LOADING;
            let audio = new Howl({
                src: ['/audio/' + ctrl.audiobookId + '/' + audiobookTrack.fileName],
                onload: function () {
                    audiobookTrack.audio = audio;
                    audiobookTrack.state = LOADED;
                    if (successCallback) successCallback();
                },
                onplay: function () {
                    audiobookTrack.state = PLAYING;
                },
                onpause: function () {
                    audiobookTrack.state = PAUSED;
                },
                onloaderror: function () {
                    audiobookTrack.state = LOAD_ERROR;
                    if (errorCallback) errorCallback();
                },
                onend: function () {
                    audiobookTrack.state = FINISHED;
                    playNext(audiobookTrack);
                }
            });
        }
    }

    let unloadTrack = function (audiobookTrack) {
        if (audiobookTrack) {
            if (audiobookTrack.audio) audiobookTrack.audio.unload();
            if (audiobookTrack.timer) {
                $interval.cancel(audiobookTrack.timer);
                audiobookTrack.timer = null;
            }
            audiobookTrack.audio = null;
            audiobookTrack.state = NOT_LOADED;
        }
    }

    let prepareNextTrack = function (audiobookTrack) {
        if (audiobookTrack) {

            if (ctrl.nextTrack) {
                unloadTrack(ctrl.nextTrack)
            }

            let index = audiobookTrack.index + 1;
            if (index <= ctrl.trackList.length) {
                let nextTrack = ctrl.trackList[index]
                loadTrack(nextTrack, function () {
                    ctrl.nextTrack = nextTrack;
                });
            }
        }
    }

    $scope.$watch(
        function () {
            return ctrl.audiobook;
        },
        function (audiobook, _) {
            if (audiobook) {
                ctrl.audiobookId = audiobook.id;
                ctrl.audiobookTitle = audiobook.title;

                let index = 0;
                let trackList = []
                let syncData = audiobook.syncData;
                let synchronizedTrack
                audiobook
                    .tracks
                    .forEach(function (track) {
                        let audiobookTrack = new AudiobookTrack(index, track);
                        if (syncData && track.fileName === syncData.fileName)
                            synchronizedTrack = audiobookTrack;
                        trackList.push(audiobookTrack);
                        index++;
                    });

                ctrl.trackList = trackList;

                if (synchronizedTrack) {
                    playPauseTrack(synchronizedTrack, syncData);
                }
            }
        }
    )
}

class AudiobookTrack {

    constructor(index, track) {
        this.index = index;
        this.fileName = track.fileName;
        this.duration = track.duration;
        this.position = 0;
        this.state = NOT_LOADED;
        this.audio = null;
    }

}