angular
    .module('applicationModule')
    .component('main', {
        templateUrl: 'frontend/application/components/main/Main.html',
        controller: MainController
    });

function MainController($http) {
    let ctrl = this;

    $http({
        method: 'GET',
        url: '/data/audiobook'
    }).then(function (response) {
        ctrl.audiobook = response.data;
    });
}